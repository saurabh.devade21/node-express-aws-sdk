const serverless = require("serverless-http");

const express = require("express");
const app = express();
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
const AWS = require('aws-sdk');


app.get("/getAllRegions", (req, res) => {
  const ec2 = new AWS.EC2({region:'eu-west-1'});

  const params = {};

  ec2.describeRegions(params, function(err, data) {
    if (err) {
      res.send({'error':err})
    } else {
      console.log(err);
      res.send({'data':data})
    }
  });
});
app.get("/getVpcsUnderRegion/:regionName", (req, res) => {
  const ec2 = new AWS.EC2({region:req.params.regionName});

  const params = {};

  ec2.describeVpcs(params, function(err, data) {
    if (err) {
     
      res.send({'error':err})
    } else {
      res.send({'data':data})
    }
  });
});

app.get("/getSubnetsUnderVpcs/:regionName/:vpcId", (req, res) => {
  const ec2 = new AWS.EC2({region:req.params.regionName});
  var params = {
    Filters: [
       {
      Name: "vpc-id", 
      Values: [
        req.params.vpcId
      ]
     }
    ]
   };

  ec2.describeSubnets(params, function(err, data) {
    if (err) {
     
      res.send({'error':err})
    } else {
      res.send({'data':data})
    }
  });
});
//app.listen(3000, () => console.log(`Listening on: 3000`));
module.exports.handler = serverless(app);





  